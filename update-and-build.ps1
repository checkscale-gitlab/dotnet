[CmdletBinding()]
Param(
    [switch]$Build,
    [switch]$Push
)

Set-Location $PSScriptRoot

@(
    'https://raw.githubusercontent.com/dotnet/dotnet-docker/master/src/runtime/3.1/nanoserver-1809/amd64/Dockerfile'
    'https://raw.githubusercontent.com/dotnet/dotnet-docker/master/src/aspnet/3.1/nanoserver-1809/amd64/Dockerfile'
    'https://raw.githubusercontent.com/dotnet/dotnet-docker/master/src/sdk/3.1/nanoserver-1809/amd64/Dockerfile'
) | ForEach-Object {

    $_ -match '/src/(.+?)/(.+?)/' | Out-Null

    $name = $Matches[1]
    $ver  = $Matches[2]

    $outFile = "./$name/Dockerfile"

    Invoke-WebRequest -Uri $_ -OutFile $outFile

    $df = Get-Content $outFile -Raw

    $df = $df -replace '\r?\n', [System.Environment]::NewLine

    $df = $df -replace 'USER .+\r?\n'

    if ($name -eq 'runtime') {
        $df = $df -replace 'FROM mcr\.microsoft\.com/windows/nanoserver:1809-amd64(\r?\n)',
                           'FROM mcr.microsoft.com/windows/servercore:ltsc2019$1'
    } else {
        $df = $df -replace [regex]::escape('FROM $REPO:3.1-nanoserver-1809'), 'FROM $REPO' `
                  -replace [regex]::escape('ARG REPO=mcr.microsoft.com/dotnet/core'), 'ARG REPO=registry.gitlab.com/reductech/containers/dotnet'
    }

    [System.IO.File]::WriteAllText($outFile, $df, [System.Text.UTF8Encoding]::new($false))

    $container = "registry.gitlab.com/reductech/containers/dotnet/$name"

    if ($Build) {
        docker build -t $container -t "${container}:${ver}" "./$name"
    }

    if ($Push) {
        docker push $container
    }

}
